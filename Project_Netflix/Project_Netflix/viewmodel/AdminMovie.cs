﻿using Project_Netflix.model;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace Project_Netflix.viewmodel
{
	public class AdminMovie : DependencyObject
	{
		public static readonly DependencyProperty AdminMovieProperty;
		public static readonly DependencyProperty SelectMovieProperty;


		public ICommand CmdAddMovie { get; set; }
		static AdminMovie()
		{
			AdminMovieProperty = DependencyProperty.Register("DSMovie", typeof(ObservableCollection<MOVIE>), typeof(AdminAccount));
			SelectMovieProperty = DependencyProperty.Register("SelectedMovie", typeof(MOVIE), typeof(AdminAccount));
		}
		public ObservableCollection<MOVIE> DSMovie
		{
			get => (ObservableCollection<MOVIE>)GetValue(AdminMovieProperty);
			set => SetValue(AdminMovieProperty, value);
		}
		public MOVIE SelectedMovie
		{
			get => (MOVIE)GetValue(SelectMovieProperty);
			set { SetValue(SelectMovieProperty, value); }
		}
		
		public AdminMovie()
		{
			MessageBox.Show("Sau khi lấy API từ IMDB sẽ thực hiện update, add movie");
			//CmdAddMovie = new CmdAddMovie();
			using (var db = new NETFLIX_DBEntities())
			{
				DSMovie = new ObservableCollection<MOVIE>(db.MOVIEs.Include("MOVIE_INFORMATION").Include("CATEGORies").ToList());
			}
		}
		public void Check()
		{
			MessageBox.Show(SelectedMovie.NAME);
		}
	}
}


