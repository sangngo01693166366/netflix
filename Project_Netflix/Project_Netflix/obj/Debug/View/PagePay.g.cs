﻿#pragma checksum "..\..\..\View\PagePay.xaml" "{8829d00f-11b8-4213-878b-770e8597ac16}" "F8D2CC6F6A05A8645F49C6CFA7534D7BC0505B541AC1EAF55831CE9FBBDCDE3E"
//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

using Project_Netflix.View;
using System;
using System.Diagnostics;
using System.Windows;
using System.Windows.Automation;
using System.Windows.Controls;
using System.Windows.Controls.Primitives;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Ink;
using System.Windows.Input;
using System.Windows.Markup;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Effects;
using System.Windows.Media.Imaging;
using System.Windows.Media.Media3D;
using System.Windows.Media.TextFormatting;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Windows.Shell;


namespace Project_Netflix.View {
    
    
    /// <summary>
    /// PagePay
    /// </summary>
    public partial class PagePay : System.Windows.Window, System.Windows.Markup.IComponentConnector {
        
        
        #line 22 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Basic;
        
        #line default
        #line hidden
        
        
        #line 23 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Standard;
        
        #line default
        #line hidden
        
        
        #line 24 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.TextBlock Premium;
        
        #line default
        #line hidden
        
        
        #line 86 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid PhimHDBasic;
        
        #line default
        #line hidden
        
        
        #line 88 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GioiHanPhimBasic;
        
        #line default
        #line hidden
        
        
        #line 90 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid TinhNang1Basic;
        
        #line default
        #line hidden
        
        
        #line 92 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid TinhNang2Basic;
        
        #line default
        #line hidden
        
        
        #line 94 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid HuyThueBasic;
        
        #line default
        #line hidden
        
        
        #line 99 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid PhimHDStandard;
        
        #line default
        #line hidden
        
        
        #line 101 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GioiHanPhimStandard;
        
        #line default
        #line hidden
        
        
        #line 103 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid TinhNang1Standard;
        
        #line default
        #line hidden
        
        
        #line 105 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid TinhNang2Standard;
        
        #line default
        #line hidden
        
        
        #line 107 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid HuyThueStandard;
        
        #line default
        #line hidden
        
        
        #line 111 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid PhimHDPremium;
        
        #line default
        #line hidden
        
        
        #line 113 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid GioiHanPhimPremium;
        
        #line default
        #line hidden
        
        
        #line 115 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid TinhNang1Premium;
        
        #line default
        #line hidden
        
        
        #line 117 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid TinhNang2Premium;
        
        #line default
        #line hidden
        
        
        #line 119 "..\..\..\View\PagePay.xaml"
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1823:AvoidUnusedPrivateFields")]
        internal System.Windows.Controls.Grid HuyThuePremium;
        
        #line default
        #line hidden
        
        private bool _contentLoaded;
        
        /// <summary>
        /// InitializeComponent
        /// </summary>
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        public void InitializeComponent() {
            if (_contentLoaded) {
                return;
            }
            _contentLoaded = true;
            System.Uri resourceLocater = new System.Uri("/Project_Netflix;component/view/pagepay.xaml", System.UriKind.Relative);
            
            #line 1 "..\..\..\View\PagePay.xaml"
            System.Windows.Application.LoadComponent(this, resourceLocater);
            
            #line default
            #line hidden
        }
        
        [System.Diagnostics.DebuggerNonUserCodeAttribute()]
        [System.CodeDom.Compiler.GeneratedCodeAttribute("PresentationBuildTasks", "4.0.0.0")]
        [System.ComponentModel.EditorBrowsableAttribute(System.ComponentModel.EditorBrowsableState.Never)]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Maintainability", "CA1502:AvoidExcessiveComplexity")]
        [System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1800:DoNotCastUnnecessarily")]
        void System.Windows.Markup.IComponentConnector.Connect(int connectionId, object target) {
            switch (connectionId)
            {
            case 1:
            this.Basic = ((System.Windows.Controls.TextBlock)(target));
            
            #line 22 "..\..\..\View\PagePay.xaml"
            this.Basic.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.TextBlock_MouseLeftButtonDown);
            
            #line default
            #line hidden
            return;
            case 2:
            this.Standard = ((System.Windows.Controls.TextBlock)(target));
            
            #line 23 "..\..\..\View\PagePay.xaml"
            this.Standard.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.TextBlock_MouseLeftButtonDown_1);
            
            #line default
            #line hidden
            return;
            case 3:
            this.Premium = ((System.Windows.Controls.TextBlock)(target));
            
            #line 24 "..\..\..\View\PagePay.xaml"
            this.Premium.MouseLeftButtonDown += new System.Windows.Input.MouseButtonEventHandler(this.TextBlock_MouseLeftButtonDown_2);
            
            #line default
            #line hidden
            return;
            case 4:
            this.PhimHDBasic = ((System.Windows.Controls.Grid)(target));
            return;
            case 5:
            this.GioiHanPhimBasic = ((System.Windows.Controls.Grid)(target));
            return;
            case 6:
            this.TinhNang1Basic = ((System.Windows.Controls.Grid)(target));
            return;
            case 7:
            this.TinhNang2Basic = ((System.Windows.Controls.Grid)(target));
            return;
            case 8:
            this.HuyThueBasic = ((System.Windows.Controls.Grid)(target));
            return;
            case 9:
            this.PhimHDStandard = ((System.Windows.Controls.Grid)(target));
            return;
            case 10:
            this.GioiHanPhimStandard = ((System.Windows.Controls.Grid)(target));
            return;
            case 11:
            this.TinhNang1Standard = ((System.Windows.Controls.Grid)(target));
            return;
            case 12:
            this.TinhNang2Standard = ((System.Windows.Controls.Grid)(target));
            return;
            case 13:
            this.HuyThueStandard = ((System.Windows.Controls.Grid)(target));
            return;
            case 14:
            this.PhimHDPremium = ((System.Windows.Controls.Grid)(target));
            return;
            case 15:
            this.GioiHanPhimPremium = ((System.Windows.Controls.Grid)(target));
            return;
            case 16:
            this.TinhNang1Premium = ((System.Windows.Controls.Grid)(target));
            return;
            case 17:
            this.TinhNang2Premium = ((System.Windows.Controls.Grid)(target));
            return;
            case 18:
            this.HuyThuePremium = ((System.Windows.Controls.Grid)(target));
            return;
            }
            this._contentLoaded = true;
        }
    }
}

